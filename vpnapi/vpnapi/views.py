from rest_framework.response import Response
from rest_framework import viewsets, renderers
from django.conf import settings
from rest_framework.views import APIView
from rest_framework.decorators import list_route
from rest_framework.reverse import reverse


class TextRenderer(renderers.BaseRenderer):
    media_type = "text/plain"
    format = "txt"

    def render(self, data, media_type=None, renderer_context=None):
        if isinstance(data, str):
            return data.encode()
        elif 'detail' in data:
            return data['detail']
        else:
            return data


class VPNView(APIView):
    def get(self, request):
        data = settings.VPN.test()
        return Response(data)


class ServerViewSet(viewsets.ViewSet):
    def list(self, request):
        data = {
            'status': reverse('server-status', request=request),
            'info': reverse('server-info', request=request),
        }
        return Response(data)

    @list_route()
    def info(self, request):
        data = settings.VPN.get_server_info()
        return Response(data)

    @list_route()
    def status(self, request):
        data = settings.VPN.get_server_status()
        return Response(data)


class ConfigViewSet(viewsets.ViewSet):
    def list(self, request):
        data = settings.VPN.get_config()
        return Response(data)

    @list_route(renderer_classes=(TextRenderer,))
    def file(self, request):
        data = settings.VPN.get_config()['FileData'][0]
        data = data.decode('utf-8')
        return Response(data)
