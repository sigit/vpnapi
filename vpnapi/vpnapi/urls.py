from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from rest_framework import routers

from .views import VPNView, ServerViewSet, ConfigViewSet

router = routers.DefaultRouter()
router.register('server', ServerViewSet, 'server')
router.register('config', ConfigViewSet, 'config')

urlpatterns = [
    url(r'^', include(router.urls)),

    url(r'^vpn/', VPNView.as_view()),
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
]
